import argparse
import random


# a^p mod b
def fast_exp(_a, _p, _b):
    _p = list(format(_p, 'b'))
    _p.reverse()
    table = {}
    for i in range(len(_p)):
        if i == 0:
            table[str(i)] = (_a ** (2 ** i)) % _b
        else:
            table[str(i)] = (table[str(i-1)]**2) % _b
    total = 1
    for i in range(len(_p)):
        if _p[i] == '1':
            total = (total * table[str(i)]) % _b
    return total


def miller_rabin(n, s=50):
    for j in range(s):
        _a = random.randrange(1, n-1)
        if witness(_a, n):
            # print('Composite')
            return False
    # print('Prime')
    return True


def witness(a, n):
    t, u = get_t_and_u(n-1)
    x = {str(0): fast_exp(a, u, n)}
    for i in range(1, t+1):
        x[str(i)] = (x[str(i-1)]**2) % n
        if x[str(i)] == 1 and x[str(i)] != 1 and x[str(i)] != -1:
            return True
    if x[str(t)] != 1:
        return True
    return False


def get_t_and_u(n):
    n_bin = list(format(n, 'b'))
    n_bin.reverse()
    # print(n_bin)
    for i in range(len(n_bin)):
        if n_bin[i] == '1':
            t = i
            break
    n_bin = n_bin[i:]
    n_bin.reverse()
    u = int(''.join(c for c in n_bin), 2)
    # print(t, u)
    return t, u


def primitive_root(p, pr):
    for i in range(1, p):
        # Ensure that i is a divisor of p-1
        n = (p-1) / i
        if n * i == (p-1):
            # Perform fast exponentiation on pr^i mod p
            if fast_exp(pr, i, p) == 1:
                return True
    return False


def get_prime():
    while True:
        q = random.randrange(low, high, 2)
        if not miller_rabin(q):
            continue
        if q % 12 == 5:
            p = 2*q + 1
            if miller_rabin(p):
                return p


def str_to_int(_buffer):
    val = 0
    for character in _buffer:
        val = val << 8
        val = ord(character) ^ val
    return val


def int_to_str(_buffer):
    _m = []
    for i in range(4):
        char = str(chr(_buffer & 255))
        if char is not '\0':
            _m.insert(0, char)
        _buffer = _buffer >> 8
    return ''.join(_m)


def pad_buffer(buffer, buflen):
    return '\0'*(4 - buflen) + buffer


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-k', '-keygen', help='use for keygen', action='store_true', default=False)
    parser.add_argument('-e', '-encrypt', help='use for encryption', action='store_true', default=False)
    parser.add_argument('-d', '-decrypt', help='use for decryption', action='store_true', default=False)
    parser.add_argument('--seed', help='provide value for seeding rand', type=int, default=False)
    parser.add_argument('--pukey', help='use for input of public key', default='pubkey.txt')
    parser.add_argument('--prkey', help='use for input of private key', default='prikey.txt')
    parser.add_argument('--pin', help='use for specific plaintext input', default=False)
    parser.add_argument('--pout', help='use for specific plaintext output', default='dtext.txt')
    parser.add_argument('--cin', help='use for specific ciphertext input', default='ctext.txt')
    parser.add_argument('--cout', help='use for specific ciphertext output', default='ctext.txt')
    args = parser.parse_args()
    if sum([1 for val in [args.k, args.e, args.d] if val]) != 1:
        print('Incorrect number of modes selected')
        print('Try: python main.py --help')
        exit(1)
    if args.k:
        print('keygen mode selected')
        low = int('40000001', 16)
        high = int('80000000', 16)
        # low = int('40000000000000000000000000000001', 16)
        # high = int('7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF', 16)
        if args.seed == 0:
            p = get_prime()
        else:
            num = random.seed(args.seed)
            p = get_prime()
        random.seed()
        g = 2
        d = random.randint(1, p)
        e2 = fast_exp(2, d, p)
        # Write public and private keys to file
        pubkey = open('pubkey.txt', 'w+')
        prikey = open('prikey.txt', 'w+')
        pubkey.write(str(p) + ' ' + str(g) + ' ' + str(e2))
        prikey.write(str(p) + ' ' + str(g) + ' ' + str(d))
        pubkey.close()
        prikey.close()
        print('Generator: 2')
    elif args.e:
        print('encrypt mode selected')
        if args.pukey and args.pin:
            # Load in files and create a cipher.txt
            pin = open(args.pin)
            pukey = open(args.pukey)
            cout = open(args.cout, 'w+')
            # Load in key
            p, g, e2 = pukey.readline().split()
            p = int(p)
            g = int(g)
            e2 = int(e2)
            # Load in 4 byes of data
            while True:
                buffer = pin.read(4)
                # Pad buffer
                buflen = len(buffer)
                if buflen < 4:
                    if buflen == 0:
                        exit(0)
                    pad_buffer(buffer, buflen)
                # Convert to int value
                val = str_to_int(buffer)
                # print(format(val, '032b'))
                # Apply encryption
                k = random.randrange(0, p)
                c1 = fast_exp(g, k, p)
                c2 = (fast_exp(e2, k, p) * val) % p
                # Write encrypted data to file
                cout.write(str(c1) + ' ' + str(c2) + '\n')
        else:
            print('Not enough arguments provided.')
            exit(-1)
    elif args.d:
        print('decrypt mode selected')
        if args.prkey and args.cin:
            # Load in files and create plaintextcopy.txt
            cin = open(args.cin)
            prkey = open(args.prkey)
            pout = open(args.pout, 'w+')
            # Load in key
            p, g, d = prkey.readline().split()
            p = int(p)
            g = int(g)
            d = int(d)
            # Load in line from cipher text
            while True:
                buffer = cin.readline()
                if len(buffer) == 0:
                    exit(0)
                c1, c2 = buffer.split()
                c1 = int(c1)
                c2 = int(c2)
                # Apply decryption
                m = (fast_exp(c1, p-1-d, p) * (c2 % p)) % p
                # print(format(m, '032b'))
                # Convert to string
                m = int_to_str(m)
                # Write decrypted data to file
                pout.write(m)
            pass
        else:
            print('Not enough arguments provided.')
            exit(-1)
